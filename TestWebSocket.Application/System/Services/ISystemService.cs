﻿namespace TestWebSocket.Application;

public interface ISystemService
{
    string GetDescription();
}
