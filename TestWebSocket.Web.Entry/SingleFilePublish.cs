﻿using Furion;
using System.Reflection;

namespace TestWebSocket.Web.Entry;

public class SingleFilePublish : ISingleFilePublish
{
    public Assembly[] IncludeAssemblies()
    {
        return Array.Empty<Assembly>();
    }

    public string[] IncludeAssemblyNames()
    {
        return new[]
        {
            "TestWebSocket.Application",
            "TestWebSocket.Core",
            "TestWebSocket.EntityFramework.Core",
            "TestWebSocket.Web.Core"
        };
    }
}