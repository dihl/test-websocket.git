﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace TestWebSocket.EntityFramework.Core;

[AppDbContext("TestWebSocket", DbProvider.Sqlite)]
public class DefaultDbContext : AppDbContext<DefaultDbContext>
{
    public DefaultDbContext(DbContextOptions<DefaultDbContext> options) : base(options)
    {
    }
}
