﻿using Furion;
using Microsoft.Extensions.DependencyInjection;

namespace TestWebSocket.EntityFramework.Core;

public class Startup : AppStartup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddDatabaseAccessor(options =>
        {
            options.AddDbPool<DefaultDbContext>();
        }, "TestWebSocket.Database.Migrations");
    }
}
